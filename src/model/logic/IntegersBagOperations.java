package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.text.html.parser.ParserDelegator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}
			
		}
		return min;
	}
	
	public double computeMedian(IntegersBag bag)
	{
		double med = 0;
		ArrayList<Integer> list = new ArrayList<Integer>();
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				list.add(value);
			}
	        for( int i = 0; i < list.size() - 1; i++ )
	        {
	            int menor = list.get(i);
	            int menorPosicion= i;

	            for( int j = i + 1; j < list.size(); j++ )
	            {
	                if( list.get(j) < menor )
	                {
	 		   menor = list.get(j);
			   menorPosicion = j;
	                }
	            }
	            int temp = list.get(i);
	            list.set(i, menor );
	            list.set(menorPosicion, temp);
	        }

			if(list.size()%2 == 1)
			{
				med = list.get((list.size()/2));
			}
			else
			{
				med = (list.get((list.size()/2)) + list.get((list.size()/2) - 1))/2;
			}
			}

		return med;
	}

	public double computeHarmonicMean(IntegersBag bag)
	{
		double hMean=0;
		double sumInverse=0;
		double inverse=0;
		int length = 0;
		
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				inverse = (double)iter.next();
				sumInverse += (1/inverse);
				length++;
			}
			if( length > 0) hMean = length / sumInverse;
		}
		return hMean;
		
	}

}
